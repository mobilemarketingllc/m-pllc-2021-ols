<?php
/**
 * Astra Child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

add_action( 'wp_enqueue_scripts', function(){

	wp_enqueue_style( 'style-name', get_stylesheet_directory_uri()."/base.min.css", array(), "", false);
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    wp_enqueue_script("script_js",get_stylesheet_directory_uri()."/script.js","","",1);
});

if ( current_user_can( 'manage_options' ) ) {
	add_filter( 'show_admin_bar', '__return_true' );
}

/**
 * Dequeue the jQuery UI styles.
 *
 * Hooked to the wp_print_styles action, with a late priority (100),
 * so that it is after the style was enqueued.
 */
function remove_pagelist_css() {
    wp_dequeue_style( 'page-list-style' );
 }
 add_action( 'wp_print_styles', 'remove_pagelist_css', 100 );

 //Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );
//Yoast SEO Breadcrumb addded
function bbtheme_yoast_breadcrumb() {
    if ( function_exists('yoast_breadcrumb') && ! is_front_page() ) {
        yoast_breadcrumb('<div id="breadcrumbs"><div class="container">','</div></div>');
    }
}
add_action( 'astra_header_after', 'bbtheme_yoast_breadcrumb' );
function new_year_number()
{
return $new_year = date('Y');
}
add_shortcode('year_code', 'new_year_number');

//Covid home page banner hook
function bbtheme_covid_banner_custom() {
    $iscovid  =  get_option('covid');
    $content ="";
     if ( $iscovid == '1' &&  is_front_page() ) {

          $content .= do_shortcode('[fl_builder_insert_layout slug="covid19-banner-row"]');
        
     }
     echo $content;
}
add_action( 'astra_header_after', 'bbtheme_covid_banner_custom' );


 //Yoast SEO Breadcrumb link - Changes for plp pages only
 add_filter( 'wpseo_breadcrumb_links', 'wpse_override_yoast_breadcrumb_trail',90 );

 function wpse_override_yoast_breadcrumb_trail( $links ) {
 
     if (is_page( 'products' )) {
        global $post;
        $post_data = get_post($post->post_parent);
        $parent_slug = $post_data->post_name;
        
         $breadcrumb[] = array(
             'url' => get_site_url().'/flooring/',
             'text' => 'Flooring',
         );
         $breadcrumb[] = array(
             'url' => get_site_url().'/flooring/'. $parent_slug .'/',
             'text' => ''. $parent_slug .'',
         );
         array_splice( $links, 1, -1, $breadcrumb );
         
     }

     if (is_page( 'gallery' )) {
        global $post;
        $post_data = get_post($post->post_parent);
        $parent_slug = $post_data->post_name;
        
         $breadcrumb[] = array(
             'url' => get_site_url().'/flooring/',
             'text' => 'Flooring',
         );
         $breadcrumb[] = array(
             'url' => get_site_url().'/flooring/'. $parent_slug .'/',
             'text' => ''. $parent_slug .'',
         );

         array_splice( $links, 1, -1, $breadcrumb );
         
     }

     if (is_page( 'installation' )) {
        global $post;
        $post_data = get_post($post->post_parent);
        $parent_slug = $post_data->post_name;
        
         $breadcrumb[] = array(
             'url' => get_site_url().'/flooring/',
             'text' => 'Flooring',
         );
         $breadcrumb[] = array(
             'url' => get_site_url().'/flooring/'. $parent_slug .'/',
             'text' => ''. $parent_slug .'',
         );

         array_splice( $links, 1, -1, $breadcrumb );
         
     }
     if (is_page( 'care-and-maintenance' )) {
        global $post;
        $post_data = get_post($post->post_parent);
        $parent_slug = $post_data->post_name;
        
         $breadcrumb[] = array(
             'url' => get_site_url().'/flooring/',
             'text' => 'Flooring',
         );
         $breadcrumb[] = array(
             'url' => get_site_url().'/flooring/'. $parent_slug .'/',
             'text' => ''. $parent_slug .'',
         );

         array_splice( $links, 1, -1, $breadcrumb );
         
     }
     if (is_page( 'floorte-hardwood' )) {
        global $post;
        $post_data = get_post($post->post_parent);
        $parent_slug = $post_data->post_name;
        
         $breadcrumb[] = array(
             'url' => get_site_url().'/flooring/',
             'text' => 'Flooring',
         );
         $breadcrumb[] = array(
             'url' => get_site_url().'/flooring/'. $parent_slug .'/',
             'text' => ''. $parent_slug .'',
         );

         array_splice( $links, 1, -1, $breadcrumb );
         
     }
     if (is_page( 'coretec-colorwall' )) {
        global $post;
        $post_data = get_post($post->post_parent);
        $parent_slug = $post_data->post_name;
        
         $breadcrumb[] = array(
             'url' => get_site_url().'/flooring/',
             'text' => 'Flooring',
         );
         $breadcrumb[] = array(
             'url' => get_site_url().'/flooring/'. $parent_slug .'/',
             'text' => ''. $parent_slug .'',
         );

         array_splice( $links, 1, -1, $breadcrumb );
         
     }
     if (is_page( 'choose-your-style' )) {
        global $post;
        $post_data = get_post($post->post_parent);
        $parent_slug = $post_data->post_name;
        
         $breadcrumb[] = array(
             'url' => get_site_url().'/flooring/',
             'text' => 'Flooring',
         );
         $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/vinyl/',
            'text' => 'vinyl',
        );
         $breadcrumb[] = array(
             'url' => get_site_url().'/flooring/'. $parent_slug .'/',
             'text' => ''. $parent_slug .'',
         );

         array_splice( $links, 1, -1, $breadcrumb );
         
     }
     if (is_page( 'rug-pads' )) {        
         $breadcrumb[] = array(
             'url' => get_site_url().'/flooring/',
             'text' => 'Flooring',
         );
         $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/area-rugs/',
            'text' => 'area-rugs',
        );

         array_splice( $links, 1, -1, $breadcrumb );
         
     }
     return $links;
 }

 function mmsession_custom_footer_js_astratheme() {
     
    echo "<script src='https://session.mm-api.agency/js/mmsession.js' async></script>";
}
add_action( 'wp_footer', 'mmsession_custom_footer_js_astratheme' );

function year_shortcode() {
    $year = date('Y');
    return $year;
}
add_shortcode('year', 'year_shortcode');
add_shortcode('fl_year', 'year_shortcode');

add_action('astra_body_bottom', 'msater_roomvo_custom_footer_js');
function msater_roomvo_custom_footer_js() {

    $website_json_data = json_decode(get_option('website_json'));

    foreach($website_json_data->sites as $site_cloud){
            
        if($site_cloud->instance == 'prod'){
    
            if( $site_cloud->roomvo == 'true'){
    
  echo "<script src='https://www.roomvo.com/static/scripts/b2b/mobilemarketing.js' async></script>";

            }
        }
    }
}


//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}


add_filter( 'auto_update_plugin', '__return_false' );

//audio eye integration
add_action('astra_body_bottom', 'astra_accessibility_audioeye_footer_js');
function astra_accessibility_audioeye_footer_js() {
    
    $social_report = json_decode(get_option('social_report'));

        foreach( $social_report as $report){

            if($report->type == 'accessibility' && $report->active == '1' && $report->platform == 'audioeye' && $report->key != ''){     

                echo $report->key;
               
            }
        }
  
}
